({
    removeFromCart : function(component, event, helper) {
        var index = event.getSource().get("v.text");  
        var accountName = component.get("v.cartItems")[index];
        var productName = accountName.productName;
        var productQuantity = accountName.productQuantity;
        var productAmount = accountName.productAmount;
        var productId = accountName.productId;
        var totalAmount = component.get("v.totalAmount");
        var totalAmt = totalAmount - (productAmount/productQuantity);
        component.set("v.totalAmount", totalAmt );
        var cartItemList = component.get("v.cartItems");
        var newCartItemList =[];
        var i;
        if(cartItemList.length != 0){
            for(i in cartItemList){
                
                if(cartItemList[i].productId == productId && cartItemList[i].productQuantity != 1)
                {   
                    var detailtemp = {};
                    detailtemp = { 'productId': '','productAmount': '','productQuantity': '','productName': ''};
                    detailtemp.productId =cartItemList[i].productId;
                    detailtemp.productAmount = cartItemList[i].productAmount - productAmount/cartItemList[i].productQuantity;
                    detailtemp.productQuantity = cartItemList[i].productQuantity - 1;
                    detailtemp.productName = cartItemList[i].productName;
                    newCartItemList.push(detailtemp);
                }
                else if(cartItemList[i].productId != productId)
                {   
                    var detailtemp = {};
                    detailtemp = { 'productId': '','productAmount': '','productQuantity': '','productName': ''};
                    detailtemp.productId =cartItemList[i].productId;
                    detailtemp.productAmount = cartItemList[i].productAmount;
                    detailtemp.productQuantity = cartItemList[i].productQuantity;
                    detailtemp.productName = cartItemList[i].productName;
                    newCartItemList.push(detailtemp);
                }
            }
            if(productQuantity == 1){
                var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Removed",
                "message": productName + ": Product removed from the Cart!",
                "type": "failure"
            });
            toastEvent.fire();
            }
        }
        component.set("v.cartItems", newCartItemList );
    },
    addToCart : function(component, event, helper) {
        
        var productItem = event.getParam("productItem");
        if(typeof(productItem) != "undefined"){
        helper.addToCartHelper(component,productItem,null);
        }
    },
    addToCart2 : function(component, event, helper) {
        
        var index = event.getSource().get("v.text");  
        var productValue = component.get("v.cartItems")[index];
        if(typeof(productValue) != "undefined"){
        helper.addToCartHelper(component,null,productValue);
        }
    },
    showFinalModal : function(component, event, helper) {
        alert('Item is successfully Placed in your Shipping Address. Thanks !');
    },
    closeModal : function(component, event, helper) {
        $A.get("e.c:ProductEvent").setParams({className1: 'slds-backdrop--',className2: 'slds-fade-in-'}).fire();
    }
})