({
    addToCartHelper : function(component,accountName,accountName2) {
        if(accountName != null){
        var productName = accountName.Name;
        var productQuantity = accountName.Quantity__c;
        var productAmount = accountName.Amount__c;
        var productId = accountName.Id;
        }
        else if(accountName2 != null)
        {
            var productName = accountName2.productName;
            var productQuantity = accountName2.productQuantity;
            var productAmount = accountName2.productAmount;
            var productId = accountName2.productId;
        }
        var totalAmount = 0;
        var cartItemList = component.get("v.cartItems");
        var newCartItemList =[];
        var i;
        if(cartItemList.length != 0){
            for(i in cartItemList){
                var detailtemp = {};
                detailtemp = { 'productId': '','productAmount': '','productQuantity': '','productName': ''};
                if(cartItemList[i].productId == productId)
                {   
                    detailtemp.productId =cartItemList[i].productId;
                    detailtemp.productAmount = cartItemList[i].productAmount + (productAmount/productQuantity);
                    detailtemp.productQuantity = cartItemList[i].productQuantity + 1;
                    detailtemp.productName = cartItemList[i].productName;
                }
                else
                {
                    detailtemp.productId =cartItemList[i].productId;
                    detailtemp.productAmount = cartItemList[i].productAmount;
                    detailtemp.productQuantity = cartItemList[i].productQuantity;
                    detailtemp.productName = cartItemList[i].productName;
                }
                newCartItemList.push(detailtemp);
            }
        }
        if(cartItemList.length == 0)
        {
            var detailtemp = {};
            detailtemp = { 'productId': '','productAmount': '','productQuantity': '','productName': ''};
            detailtemp.productId = productId;
            detailtemp.productAmount = productAmount;
            detailtemp.productQuantity = 1;
            detailtemp.productName = productName;
            newCartItemList.push(detailtemp);
        }
        else
        {   
            var detailtemp = {};
            detailtemp = { 'productId': '','productAmount': '','productQuantity': '','productName': ''};
            var checkExisting = '';
            for(i in newCartItemList){
                if(newCartItemList[i].productId == productId){
                    checkExisting = 'true';
                }
            }
            if(checkExisting != 'true')
            {
                detailtemp.productId = productId;
                detailtemp.productAmount = productAmount;
                detailtemp.productQuantity = 1;
                detailtemp.productName = productName;
                newCartItemList.push(detailtemp);
            }
        }
        component.set("v.cartItems", newCartItemList );
        if(newCartItemList.length != 0){
            for(i in newCartItemList){
                totalAmount = totalAmount+newCartItemList[i].productAmount;
            }
            component.set("v.totalAmount", totalAmount );
        }
        else
        {
            totalAmount = productAmount;
        }
        if(accountName != null){
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Success",
                "message": accountName.Name + ": Product added to Cart!",
                "type": "success"
            });
            toastEvent.fire();
        }
    }
})