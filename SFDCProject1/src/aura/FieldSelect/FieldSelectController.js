({
	init : function(component, event, helper) {
        if ($A.util.isEmpty(component.get("v.options"))) {
		helper.getPicklistValues(component);
        }
        if ($A.util.isEmpty(component.get("v.label"))) {
            helper.getLabel(component);
        }
	}
})