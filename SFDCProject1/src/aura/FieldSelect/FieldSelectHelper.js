({
	getPicklistValues : function(component) {
    	var sObject = component.get("v.sObject");
        var fieldName = component.get("v.fieldName");
        if ($A.util.isEmpty(sObject) || $A.util.isEmpty(fieldName)) {
            return;
        }
        
        var action = component.get("c.getSelectOptions");
        action.setParam("sobj", sObject);
        action.setParam("fieldName", fieldName);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var ret = response.getReturnValue();
                var options = [];
                for (var i = 0; i < ret.length; i++) {
                    options.push({ label: ret[i][0], value: ret[i][1] });
                }
                component.set("v.options", options);
            }
            else if (component.isValid() && state === "INCOMPLETE") {
            }
                else if (component.isValid() && state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
	},
    
	getLabel : function(component) {
    	var sObject = component.get("v.sObject");
        var fieldName = component.get("v.fieldName");
        if ($A.util.isEmpty(sObject) || $A.util.isEmpty(fieldName)) {
            return;
        }
        
        var action = component.get("c.getLabel");
        action.setParam("sobj", sObject);
        action.setParam("fieldName", fieldName);
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                var ret = response.getReturnValue();
                component.set("v.label", ret);
            }
            else if (component.isValid() && state === "INCOMPLETE") {
            }
                else if (component.isValid() && state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
        });
        $A.enqueueAction(action);
	}
})