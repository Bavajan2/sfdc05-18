({
    keydown : function(component, event, helper) {
        if (event.keyCode == 13) {
            $A.get("e.c:ProductEvent").setParams({"value": component.get("v.value")}).fire();
            event.target.value = '';
            component.set("v.value", "");
            var modal = component.find('btnAllProd');
            $A.util.removeClass(modal,'hide');
        }
    },
    
    input : function(component, event, helper) {
        component.set("v.value", event.target.value);
    },
    
    restAll : function(component, event, helper) {
        $A.get("e.c:ProductEvent").setParams({"value": null}).fire();
        var modal = component.find('btnAllProd');
        $A.util.addClass(modal,'hide');
    }
})