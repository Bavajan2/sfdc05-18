({
	closeModal : function(component, event, helper) {
        $A.get("e.c:ProductEvent").setParams({className1: 'slds-backdrop--',className2: 'slds-fade-in-'}).fire();
    },
    addToCart : function(component, event, helper) {
        var productToadd = component.get("v.productItem");
        $A.get("e.c:ProductEvent").setParams({productItem : productToadd,className1: 'slds-backdrop--',className2: 'slds-fade-in-'}).fire();
    }
})