({
    previousPage : function(component, event, helper) {
        var myEvent = $A.get("e.c:PageChange");
        myEvent.setParams({ "direction": "previous"});
        myEvent.fire();
    },
    nextPage : function(component, event, helper) {
        var myEvent = $A.get("e.c:PageChange");
        myEvent.setParams({ "direction": "next"});
        myEvent.fire();
    },
    onSelectChange : function(component, event, helper) {
        var selected = component.find("levels").get("v.value");
        var myEvent = $A.get("e.c:PageChange");
        myEvent.setParams({ "pageSize": selected}).fire();
    }
})