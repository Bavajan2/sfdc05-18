({
    remove : function(component, event, helper) {
        
        var pills = component.get("v.pills");
        pills.splice(event.target.id, 1);
        $A.get("e.c:ProductEvent").setParams({filters: pills}).fire();
        component.set("v.pills", pills);
    }
})