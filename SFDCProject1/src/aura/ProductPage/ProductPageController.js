({
    doInit : function(component, event, helper) {
        helper.getsObjectRecords(component);
        helper.getObjectLabel(component);
        helper.getProductType(component);
        helper.getDependentPicklistValues(component);
    },
    showSpinner : function (component, event, helper) {
        component.set("v.Spinner", true); 
    },
    
    hideSpinner : function (component, event, helper) {
        component.set("v.Spinner", false);
    },
    SelctCheckbox : function(component, event, helper) {
        helper.getsObjectRecords(component);
    },
    handleCheckTask : function(component, event, helper) {
        var index = event.getSource().get("v.text");  
        var prodId = component.get("v.latestRecords")[index];
        component.set("v.productItem",prodId);
        helper.toggleClass(component,'backdrop','slds-backdrop--');
        helper.toggleClass(component,'modaldialog','slds-fade-in-');
    },
    closeModal : function(component, event, helper) {
        var value = event.getParam("value");
        var className1 = event.getParam("className1");
        var className2 = event.getParam("className2");
        var filters = event.getParam("filters");
        helper.toggleClassInverse(component,'backdrop',className1);
        helper.toggleClassInverse(component,'modaldialog',className2);
        if(typeof value != 'undefined')
        {
            component.set("v.value",value);
            helper.getsObjectRecords(component);
        }
        if(typeof filters != 'undefined')
        {
            component.set("v.filters",filters);
            helper.getsObjectRecords(component);
        }
    },
    changeSubCategory: function(component, event, helper) {
        var activeSearchFilters = component.get("v.selectedSubCategory");
        var filtersvalues = component.get("v.filters");
        filtersvalues.push(activeSearchFilters);
        component.set("v.filters", filtersvalues);
        if(!$A.util.isEmpty(component.get("v.filters")))
            helper.getsObjectRecords(component);
    },
    addToCart : function(component, event, helper) {
        var index = event.getSource().get("v.text");  
        var productToadd = component.get("v.latestRecords")[index];
        $A.get("e.c:ProductEvent").setParams({productItem : productToadd}).fire();
    },
    showCart : function(component, event, helper) {
        var modal = component.find('itemSec');
        var modal2 = component.find('CartSec');
        var modal3 = component.find('showButton');
        var modal4 = component.find('hideButton');
        $A.util.removeClass(modal,'slds-size--5-of-6');
        $A.util.addClass(modal,'slds-size--3-of-6');
        $A.util.addClass(modal2,'slds-size--2-of-6');
        $A.util.removeClass(modal2,'hidePanel');
        
        $A.util.removeClass(modal4,'hideButton');
        $A.util.addClass(modal3,'hideButton');
    },
    closeCart : function(component, event, helper) {
        var modal = component.find('itemSec');
        var modal2 = component.find('CartSec');
        var modal3 = component.find('showButton');
        var modal4 = component.find('hideButton');
        $A.util.addClass(modal,'slds-size--5-of-6');
        $A.util.removeClass(modal,'slds-size--3-of-6');
        $A.util.removeClass(modal2,'slds-size--2-of-6');
        $A.util.addClass(modal2,'hidePanel');
        
        $A.util.addClass(modal4,'hideButton');
        $A.util.removeClass(modal3,'hideButton');
    },
    pageChange: function(component, event, helper) {
        var pageS = event.getParam("pageSize");
        if(typeof pageS!='undefined')
            component.set("v.pageSize",pageS);
        var page = component.get("v.page") || 1;
        var direction = event.getParam("direction");
        if(typeof direction != 'undefined')
        {
            page = direction === "previous" ? (page - 1) : (page + 1);
            helper.getsObjectRecords(component,page);
        }
        else
        {
            helper.getsObjectRecords(component);
        }
    }
})