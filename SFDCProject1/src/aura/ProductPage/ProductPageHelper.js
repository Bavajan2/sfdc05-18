({
    getsObjectRecords : function(component,page) {
        
        var prodTypeList = component.get("v.prodType");
        var prodList = '';
        if(!$A.util.isEmpty(prodTypeList) && !$A.util.isUndefined(prodTypeList)){
            prodList = JSON.stringify(prodTypeList);
        }
        var page = page || 1;
        var action = component.get("c.getRecords");
        var fields = component.get("v.fields");
        var subCatList = null;
        //Get the Dependent Picklist Value
        var i,key;
        var selectProdType = [];
        var dependentPicklistValue = [];
        var abc = [];
        var mymap = component.get("v.ControllingAndDependentValues");
        for(i in prodTypeList){
            if(prodTypeList[i].isSelected == true){
                selectProdType.push(prodTypeList[i].prodType);
                dependentPicklistValue.push(mymap[prodTypeList[i].prodType]);
            }
        }
        for(var i=0;i<dependentPicklistValue.length;i++){
            for(var x=0;x<dependentPicklistValue[i].length;x++)
            abc.push(dependentPicklistValue[i][x]);
        }
        component.set("v.options",abc);
        
        //End
        if(!$A.util.isEmpty(component.get("v.filters")) && !$A.util.isUndefined(component.get("v.filters"))){
            subCatList = JSON.stringify(component.get("v.filters"));
        }
        action.setParams({
            ObjectName : component.get("v.object"),
            fieldstoget : fields.join(),
            pageNumber : page,
            pageSize : component.get("v.pageSize"),
            prodList: prodList ,
            filters : subCatList,
            value : component.get("v.value")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === 'SUCCESS'){
                // component.set("v.latestRecords",response.getReturnValue());
                component.set("v.page",page);
                var retResponse = response.getReturnValue();
                var retRecords = retResponse.sObjectrecords;
                component.set("v.total",retResponse.total);
                component.set("v.pages",Math.ceil(retResponse.total/component.get("v.pageSize")));
                component.set("v.latestRecords",retRecords);
            }else if (state === "ERROR") {
                console.log('Error');
            }
        });
        $A.enqueueAction(action);
        
    },
    getObjectLabel : function(component){
        var action = component.get("c.getProductLabelName"); 
        action.setParams({
            tableHeader : component.get("v.fields"),
            ObjectName : component.get("v.object")
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.fieldsName", a.getReturnValue());
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        
        $A.enqueueAction(action);
        
    },
    getProductType : function(component)
    {
        var action2 = component.get("c.getProductType");        
        action2.setCallback(this, function(a2) {
            if (a2.getState() === "SUCCESS") {
                component.set("v.prodType", a2.getReturnValue());
            } else if (a2.getState() === "ERROR") {
                $A.log("Errors", a2.getError());
            }
        });
        
        $A.enqueueAction(action2);
    },
    toggleClass: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.removeClass(modal,className+'hide');
        $A.util.addClass(modal,className+'open');
    },
    
    toggleClassInverse: function(component,componentId,className) {
        var modal = component.find(componentId);
        $A.util.addClass(modal,className+'hide');
        $A.util.removeClass(modal,className+'open');
    },
    getDependentPicklistValues : function(component) {
        var sObject = component.get("v.object");
        var controllingfieldName = 'Category__c';
        var dependentfieldName = 'Sub_Category__c';
        
        var action = component.get("c.getDependentOptions");
        action.setParams({
            pObjName : component.get("v.object"),
            pControllingFieldName : controllingfieldName,
            pDependentFieldName : dependentfieldName
        });
        action.setCallback(this, function(a) {
            if (a.getState() === "SUCCESS") {
                component.set("v.ControllingAndDependentValues", a.getReturnValue());
                console.log(JSON.stringify(component.get("v.ControllingAndDependentValues"))+'@Hello');
            } else if (a.getState() === "ERROR") {
                $A.log("Errors", a.getError());
            }
        });
        $A.enqueueAction(action);
    }
})