public with sharing class CasepageCls {

    public CasepageCls(ApexPages.StandardController controller) {
    this.cs = (Case) controller.getRecord();

    }


public Boolean show { get; set; }
public Case cs { get; set; }

public PageReference saveValue() {
        insert cs;
        PageReference  ref=new PageReference('/apex/casePage');
        ref.setRedirect(true);    
        return ref;
   }

public PageReference cancelValue() {
       
        PageReference  ref=new PageReference('/500/o');
           
        return ref;
   }

public PageReference displayPage() {

        if (cs.reason=='breakdown'){
        show =true;
        }
        else
        show =false;
        return null;
   }



public CasepageCls(){
    cs=new case();
    show =false;

    } 
}