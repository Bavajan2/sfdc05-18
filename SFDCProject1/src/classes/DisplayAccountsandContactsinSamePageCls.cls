public with sharing class DisplayAccountsandContactsinSamePageCls {

    public List<wrapper> items { get; set; }
    
    public class wrapper{
    
    public Account acc { get; set; }
    public Contact con { get; set; }
    
    public wrapper(account a,contact c){
    this.acc=a;
    this.con=c;
    }
    }
    
     public DisplayAccountsandContactsinSamePageCls (){
    items =new List<wrapper>();
    List<account> acclst=[select id,name from account limit 10];
    List<contact> conlst=[select id ,name from contact limit 10];
    for(integer i=0; i<10; i++){
    items.add(new wrapper(acclst[i],conlst[i]));
    }
       
    }
}