public with sharing class EditDeleteCls {
//public string rowNo { get; set; }
public string rowIndex { get; set; }

    public PageReference delProd() {
    
    Prod=[Select id,name from Product2 where id=:rowIndex];
    system.debug('+++'+rowIndex);
    system.debug('+++'+Prod);
    delete Prod;
     system.debug('****'+Prod);
    pagereference ref = new pagereference('/apex/EditDelete');
        ref.setredirect(True);
        return ref;      
    }

    public List<Product2> Prod { get; set; }
    
    
    Public EditDeleteCls (){
    
    Prod=[Select id,name from Product2];
    
    }


}