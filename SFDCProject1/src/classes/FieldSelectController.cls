public class FieldSelectController {
    
  
    @AuraEnabled
    public static List<List<String>> getSelectOptions(String sobj, String fieldName) {
        List<List<String>> options = new List<List<String>>();
        Schema.DescribeFieldResult d = Schema.getGlobalDescribe().get(sobj).getDescribe().fields.getMap()
            .get(fieldName).getDescribe();
        
        List<Schema.PicklistEntry> pv = d.getPickListValues();
        for (Schema.PicklistEntry a : pv) {
            List<String> l = new List<String>();
            l.add(a.getLabel());
            l.add(a.getValue());
            options.add(l);
        }
        
        return options;
    }
    
    @AuraEnabled
    public static String getLabel(String sobj, String fieldName) {
        Schema.DescribeFieldResult d = Schema.getGlobalDescribe().get(sobj).getDescribe().fields.getMap()
            .get(fieldName).getDescribe();
        
        return d.getLabel();
    }
    
    
}