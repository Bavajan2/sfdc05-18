public with sharing class LightningCustomPage{
    
   
    
    @AuraEnabled
    public static List<String> getProductLabelName(List<String> tableHeader,string ObjectName) {
    List<String> finalTableHeader = new List<String>();
    List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{ObjectName}); // this can accept list of strings, we describe only one object here
    String objectLabel2 = describeSobjectsResult[0].getLabel();
    Map<String, Schema.SObjectField> allFields = describeSobjectsResult[0].fields.getMap();
    for(string str : tableHeader)
    {
        finalTableHeader.add(allFields.get(str).getDescribe().getLabel());
    }
    return finalTableHeader;
    }
    
    /*
    Method to Get the Category Picklist values using Schema Describe
    */
    @AuraEnabled
    public static List<LightningCustomPage.DisplayProdFamilyRecords> getProductType() {
    
    set<string> setAccType = new set<string>();
    Schema.DescribeFieldResult fieldResult =
    Product__c.Category__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    
    for( Schema.PicklistEntry f : ple)
    {
        setAccType.add(f.getValue());
    }
        
    string[] strProducttype = new List<String>();

    for(string str : setAccType){
       strProducttype.add(str); 
    }
    
    List<LightningCustomPage.DisplayProdFamilyRecords> lstAccRecords = 
        new List<LightningCustomPage.DisplayProdFamilyRecords>();
        
        
        if(strProducttype!= null && strProducttype.size() > 0){
            
            for(String acc: strProducttype){
                
                LightningCustomPage.DisplayProdFamilyRecords accRecord = 
                new LightningCustomPage.DisplayProdFamilyRecords();
                accRecord.prodType= acc;
                
                lstAccRecords.add(accRecord );
            }
            
        }  
    return lstAccRecords ;
    }
    
    public class DisplayProdFamilyRecords {
        
        @AuraEnabled public boolean isSelected;
        @AuraEnabled public string prodType;
        
        public DisplayProdFamilyRecords(){
            isSelected = false;
            prodType= '';
        }
    }
    
    /*
    Method to Get the Records based on Parameter based from Aura Component Controller
    */
    @AuraEnabled 
    public static lightningTableWrapper getRecords(String ObjectName,String fieldstoget,decimal pageNumber, String pageSize,string prodList,List<String> filters,string value){     
        system.debug(ObjectName+'@@==@@'+fieldstoget+'@@==@@'+pageNumber+'@@==@@'+pageSize+'@@==@@'+prodList+'123@@==='+filters.isEmpty()+'@@@=='+filters+'@@value@@'+value);
        
        set<String> filtersCategory = new set<String>();
        filtersCategory.addAll(filters);
        
        integer offset = (integer.valueOf(pageNumber) - 1) * integer.valueof(pageSize);
        String queryString = 'Select '+ String.escapeSingleQuotes(fieldstoget)+
                             ' from '+ String.escapeSingleQuotes(ObjectName) ;
        
        //Condition for Filter                     
        String whereClause = ' WHERE ';
        string whereclausequery;
        
        
        if(!string.isBlank(prodList) )
        {   
            List<LightningCustomPage.DisplayProdFamilyRecords> lstProdRecords = 
            (List<LightningCustomPage.DisplayProdFamilyRecords>)
            System.JSON.deserialize(prodList,List<LightningCustomPage.DisplayProdFamilyRecords>.class);
            integer i = 0;
            integer x =0;
            string soqlQuery = '';
            for(LightningCustomPage.DisplayProdFamilyRecords objProdRecords:lstProdRecords)
            { 
                if(objProdRecords.isSelected && i == 0)
                {                 
                    soqlQuery += ' Category__c =\''+objProdRecords.prodType+'\'';
                     
                    i=1;
                    x=1;    
                    
                }
                else if(objProdRecords.isSelected && i == 1)
                {
                    soqlQuery += ' OR Category__c =\''+objProdRecords.prodType+'\'';
                    x=2;
                }
            }
            
            system.debug('===>soqlQuery'+soqlQuery);
            if(x == 1){
                whereclausequery = whereClause +  soqlQuery;  
            } else if(x==2){
                whereclausequery = whereClause + '('+soqlQuery+')';
            } 
        }
        if(String.isNotBlank(whereclausequery)) {
            queryString += whereclausequery ;
            system.debug('Query::'+queryString );
        }
        if(filtersCategory.size() > 0){
            queryString += ' AND Sub_Category__c =: filters ';
        }
        if(!string.isBlank(whereclausequery) && value != null){
            queryString += ' AND';
        }
        else if(value != null)
        {
            queryString += ' WHERE';
        }
        if(value != null){
            queryString += ' (Name LIKE \'%'+value+'%\'';
            queryString += 'OR Sub_Category__c LIKE \'%'+value+'%\'';
            queryString += 'OR Category__c LIKE \'%'+value+'%\')';
        }
        if(String.isNotBlank(queryString )) {
            queryString += ' Limit '+ String.escapeSingleQuotes(pageSize) + ' Offset '+offset;  
            system.debug('Final Query::'+queryString );
        }    
        lightningTableWrapper ltw = new lightningTableWrapper();
        try
        {
            ltw.total = database.countQuery('Select count() from '+String.escapeSingleQuotes(ObjectName));
            ltw.page = Integer.valueOf(pageNumber);
            ltw.sObjectrecords  = database.query(queryString);
        }
        catch(Exception ae)
        {
            system.debug('Exception : '+ae);
        }
        return ltw;
    }
    
    /*
    Method to Get the Dependent Picklist values from Controlling Field
    */
    @AuraEnabled
    public static Map<String,List<String>>  getDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName){
        Map<String,List<String>> objResults = new Map<String,List<String>>();
        //get the string to sobject global map
        Map<String,Schema.SObjectType> objGlobalMap = Schema.getGlobalDescribe();
        if (!objGlobalMap.containsKey(pObjName))
            return objResults ;
        //get the type being dealt with
        Schema.SObjectType pType = objGlobalMap.get(pObjName);
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        //verify field names
        if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName))
            return objResults ;   
        //get the control values   
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        //get the dependent values
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();
        //iterate through the values and get the ones valid for the controlling field name
        Bitset objBitSet = new Bitset();
        //set up the results
        for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){           
            //get the pointer to the entry
            Schema.PicklistEntry ctrl_entry = ctrl_ple[pControllingIndex];
            //get the label
            String pControllingLabel = ctrl_entry.getLabel();
            //create the entry with the label
            objResults.put(pControllingLabel,new List<String>());
        }
        for(Integer pDependentIndex=0; pDependentIndex<dep_ple.size(); pDependentIndex++){          
            //get the pointer to the dependent index
            Schema.PicklistEntry dep_entry = dep_ple[pDependentIndex];
            //get the valid for
            String pEntryStructure = JSON.serialize(dep_entry);                
            TPicklistEntry objDepPLE = (TPicklistEntry)JSON.deserialize(pEntryStructure, TPicklistEntry.class);
            //if valid for is empty, skip
            if (objDepPLE.validFor==null || objDepPLE.validFor==''){
                continue;
            }
            //iterate through the controlling values
            for(Integer pControllingIndex=0; pControllingIndex<ctrl_ple.size(); pControllingIndex++){    
                if (objBitSet.testBit(objDepPLE.validFor,pControllingIndex)){                   
                    //get the label
                    String pControllingLabel = ctrl_ple[pControllingIndex].getLabel();
                    objResults.get(pControllingLabel).add(objDepPLE.label);
                }
            }
        } 
        return objResults;
    }
    
    
    
    public class lightningTableWrapper {
    @AuraEnabled
    public Integer page { get;set; }
    @AuraEnabled
    public Integer total { get;set; }
    @AuraEnabled
    public List<sObject> sObjectrecords { get;set; }
    }
    
    
    public class TPicklistEntry{
        public string active {get;set;}
        public string defaultValue {get;set;}
        public string label {get;set;}
        public string value {get;set;}
        public string validFor {get;set;}
        public TPicklistEntry(){
            
        }
    }  
    
}