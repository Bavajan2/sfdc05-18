@isTest 
private class LightningCustomPageTestClass {
    static testMethod void LightningCustomPageMethod() {
    
    string ObjectName = 'Product__c';
    String pControllingFieldName = 'Category__c';
    String pDependentFieldName= 'Sub_Category__c';
    string fieldstoget = 'Name';
    decimal pageNumber = 1;
    string pageSize = '4';
    string prodList = '[{"isSelected":true,"prodType":"Electronics"},{"isSelected":true,"prodType":"Clothing"},{"isSelected":true,"prodType":"Home Appliances"},{"isSelected":false,"prodType":"Sports and Fitness"}]';
    string value = 'test';
    string filter1 ='Android';
    string filter2 ='IOS';
    List<string> filters= new List<string>();
    filters.add(filter1);
    filters.add(filter2);
    
    
    string tableHeaderEle1 ='Name';
    string tableHeaderEle2='Amount__c';
    List<string> tableHeader= new List<string>();
    tableHeader.add(tableHeaderEle1);
    tableHeader.add(tableHeaderEle2);
     
    LightningCustomPage.getRecords(ObjectName,fieldstoget,pageNumber,pageSize,prodList,filters,value);
    
    LightningCustomPage.getDependentOptions(ObjectName, pControllingFieldName, pDependentFieldName);
    
    LightningCustomPage.getProductLabelName(tableHeader,ObjectName);
    
    LightningCustomPage.getProductType();
    }
}