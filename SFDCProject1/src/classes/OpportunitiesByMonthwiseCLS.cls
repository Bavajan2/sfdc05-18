public class OpportunitiesByMonthwiseCLS {

 public List <OpportunityInfo> opportunityList{get;set;}
    //Constructor
    public OpportunitiesByMonthwiseCLS(ApexPages.StandardController controller) 
    {
        opportunityList = new list <OpportunityInfo>();
        for(integer i=0;i<System.now().month();i++)
        {
            OpportunityInfo oinfo = new OpportunityInfo();
            oinfo.month = datetime.newinstance(2013,1,1).addmonths(i).format('MMM');
            oinfo.count = 0;
            opportunityList.add(oinfo );
        }
        AggregateResult[] groupedResults  = [SELECT Count(id)monthCount,CALENDAR_MONTH(createdDate)                                                                monthNo FROM Opportunity WHERE createdDate = THIS_YEAR GROUP BY CALENDAR_MONTH(createdDate)];
        
        for (AggregateResult ag : groupedResults)  
        {
            Integer MonthNo = integer.valueof(ag.get('monthNo'));
            Integer MonthCount = integer.valueOf(ag.get('monthCount'));
            opportunityList.get(MonthNo-1).count = MonthCount;
        }                
    }
    //Wrapper Class
    public class OpportunityInfo
    {
        public String month{get;set;}
        public integer count{get;set;}    
    }
    
    
}