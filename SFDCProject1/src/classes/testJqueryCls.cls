public with sharing class testJqueryCls {

    public boolean msg { get; set; }
    public String text { get; set; }
    public List<bav__Running_Past_loan__c> items { get; set; }
    public bav__Running_Past_loan__c RunningObj { get; set; }
    
    public PageReference saveLoan() {
        //accountObj=new Account();
        
        //system.debug('accountObj'+accountObj);
        insert RunningObj;
        PageReference ref=new PageReference ('/apex/testJquery');
        ref.setRedirect(true);
        return ref;
    }
    
    public void Showall() {
        items=[select id,name from bav__Running_Past_loan__c limit 15];
        //text='';
        msg=false;
     
    }


    public PageReference search() {
        
        if(items.size()==0){
            msg=true;
        }
        else{
            string squery='select id,name from bav__Running_Past_loan__c where name like \''+text+'%'+'\'';
            items=database.query(squery);
            msg=false;
        }
        return null;
    }


    public testJqueryCls(){
        RunningObj =new bav__Running_Past_loan__c();
        items=[select id,name from bav__Running_Past_loan__c limit 8];
        msg=false;
    }
    
}