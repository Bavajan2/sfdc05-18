public with sharing class testJqueryClss {

    public boolean msg { get; set; }
    public String text { get; set; }
    public List<Account> items { get; set; }
    public Account accountObj { get; set; }
    
    public PageReference saveAccount() {
        //accountObj=new Account();
        
        //system.debug('accountObj'+accountObj);
        insert accountObj;
        PageReference ref=new PageReference ('/apex/testJquery');
        ref.setRedirect(true);
        return ref;
    }
    
    public void Showall() {
        items=[select id,name from Account limit 15];
        //text='';
        msg=false;
     
    }


    public PageReference search() {
        
        if(items.size()==0){
            msg=true;
        }
        else{
            string squery='select id,name from Account where name like \''+text+'%'+'\'';
            items=database.query(squery);
            msg=false;
        }
        return null;
    }


    public testJqueryClss(){
        accountObj =new Account();
        items=[select id,name from Account limit 8];
        msg=false;
    }
    
}